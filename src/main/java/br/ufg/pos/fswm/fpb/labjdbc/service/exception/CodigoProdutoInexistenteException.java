package br.ufg.pos.fswm.fpb.labjdbc.service.exception;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 24/06/17.
 */
public class CodigoProdutoInexistenteException extends Exception {
    public CodigoProdutoInexistenteException(String message) {
        super(message);
    }
}
