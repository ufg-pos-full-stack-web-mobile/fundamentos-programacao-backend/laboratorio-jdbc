package br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions;

import java.sql.SQLException;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 23/06/17.
 */
public class InclusaoProdutoException extends Exception {

    public InclusaoProdutoException(String message, Throwable exception) {
        super(message, exception);
    }
}
