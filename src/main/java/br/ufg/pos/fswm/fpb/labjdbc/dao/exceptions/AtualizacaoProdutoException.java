package br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions;

import java.sql.SQLException;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 23/06/17.
 */
public class AtualizacaoProdutoException extends Throwable {
    public AtualizacaoProdutoException(String message, SQLException e) {
        super(message, e);
    }
}
