package br.ufg.pos.fswm.fpb.labjdbc.application;

import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.AtualizacaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.DelecaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.InclusaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.model.Produto;
import br.ufg.pos.fswm.fpb.labjdbc.service.ProdutoService;
import br.ufg.pos.fswm.fpb.labjdbc.service.exception.CodigoProdutoInexistenteException;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 24/06/17.
 */
public class Principal {

    /*
     * Garanta que o banco estará criado.
     */
    public static void main(String... args) {
        final ProdutoService service = new ProdutoService();

        final Produto primeiroProduto = new Produto("Arroz");
        cadastrarNovoProduto(service, primeiroProduto);

        final Produto segundoProduto = new Produto("Feijão");
        cadastrarNovoProduto(service, segundoProduto);

        final Produto terceiroProduto = new Produto("Carne");
        cadastrarNovoProduto(service, terceiroProduto);

        System.out.println("Listando todos os produtos");
        final List<Produto> produtos = service.buscarTodos();
        for (Produto produto : produtos) {
            System.out.println(produto.toString());
        }

        System.out.println();
        System.out.println("Consultando um produto no banco");
        final Produto produtoConsultado = service.buscarPorId(3L);
        System.out.println(produtoConsultado);

        System.out.println();
        System.out.println("Atualizando o produto consultado");
        produtoConsultado.setNome("Tapioca");
        atualizarProduto(service, produtoConsultado);

        System.out.println();
        System.out.println("Consultando o produto atualizado no banco");
        final Produto produtoAtualizado = service.buscarPorId(3L);
        System.out.println(produtoAtualizado.toString());

        System.out.println();
        System.out.println("Deletando um produto");
        excluirProduto(service, 2L);

        System.out.println();
        System.out.println("Mostrando nova lista");
        final List<Produto> novosProdutos = service.buscarTodos();
        for (Produto novosProduto : novosProdutos) {
            System.out.println(novosProduto.toString());
        }
    }

    private static void excluirProduto(ProdutoService service, Long id) {
        try {
            System.out.println("Excluindo produto");
            service.excluir(id);
            System.out.println("Produto excluído com sucesso");
        } catch (CodigoProdutoInexistenteException e) {
            System.err.println(e.getMessage());
        } catch (DelecaoProdutoException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void atualizarProduto(ProdutoService service, Produto produtoConsultado) {
        try {
            System.out.println("Atualizando produto");
            service.atualizar(produtoConsultado);
            System.out.println("Produto atualizado com sucesso");
        } catch (AtualizacaoProdutoException e) {
            System.err.println(e.getMessage());
        } catch (InclusaoProdutoException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void cadastrarNovoProduto(ProdutoService service, Produto primeiroProduto) {
        try {
            System.out.println("Cadastrando produto");
            service.cadastrarNovo(primeiroProduto);
            System.out.println("Produto cadastrado com sucesso");
        } catch (InclusaoProdutoException e) {
            System.err.println(e.getMessage());
        } catch (AtualizacaoProdutoException e) {
            System.err.println(e.getMessage());
        }
    }

}
