package br.ufg.pos.fswm.fpb.labjdbc.service;

import br.ufg.pos.fswm.fpb.labjdbc.dao.ProdutoDAO;
import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.AtualizacaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.DelecaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.InclusaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.dao.impl.ProdutoDAOImpl;
import br.ufg.pos.fswm.fpb.labjdbc.model.Produto;
import br.ufg.pos.fswm.fpb.labjdbc.service.exception.CodigoProdutoInexistenteException;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 23/06/17.
 */
public class ProdutoService {

    private ProdutoDAO produtoDao;

    public ProdutoService() {
        produtoDao = new ProdutoDAOImpl();
    }

    public void cadastrarNovo(Produto produto) throws InclusaoProdutoException, AtualizacaoProdutoException {
        if (produto.getId() != null) {
            produtoDao.atualizar(produto);
        } else {
            produtoDao.incluir(produto);
        }
    }

    public void atualizar(Produto produto) throws AtualizacaoProdutoException, InclusaoProdutoException {
        if (produto.getId() == null) {
            throw new AtualizacaoProdutoException("Não é possível atualizar produto sem ID", null);
        }

        final Produto doBanco = buscarPorId(produto.getId());

        if (doBanco == null) {
            throw new AtualizacaoProdutoException("Não existe produto com ID " + produto.getId() + " para ser atualizado", null);
        }

        produtoDao.atualizar(produto);
    }

    public void excluir(Long id) throws CodigoProdutoInexistenteException, DelecaoProdutoException {
        final Produto produto = buscarPorId(id);

        if (produto == null) {
            throw new CodigoProdutoInexistenteException("Produto com id " + id + " não existe");
        }

        produtoDao.excluir(id);
    }

    public Produto buscarPorId(Long id) {
        return produtoDao.buscarPorId(id);
    }

    public List<Produto> buscarTodos() {
        return produtoDao.buscarTodos();
    }

}
