package br.ufg.pos.fswm.fpb.labjdbc.dao;

import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.AtualizacaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.DelecaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.InclusaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.model.Produto;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 23/06/17.
 */
public interface ProdutoDAO {

    void incluir(Produto produto) throws InclusaoProdutoException;

    void atualizar(Produto produto) throws AtualizacaoProdutoException;

    Produto buscarPorId(Long id);

    List<Produto> buscarTodos();

    void excluir(Long id) throws DelecaoProdutoException;
}
