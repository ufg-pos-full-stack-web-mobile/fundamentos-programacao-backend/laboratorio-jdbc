package br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions;

import java.sql.SQLException;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 24/06/17.
 */
public class DelecaoProdutoException extends Exception {
    public DelecaoProdutoException(String message, SQLException e) {
        super(message, e);
    }
}
