package br.ufg.pos.fswm.fpb.labjdbc.dao.impl;

import br.ufg.pos.fswm.fpb.labjdbc.dao.ProdutoDAO;
import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.AtualizacaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.DelecaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.dao.exceptions.InclusaoProdutoException;
import br.ufg.pos.fswm.fpb.labjdbc.model.Produto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 23/06/17.
 */
public class ProdutoDAOImpl implements ProdutoDAO {

    public void incluir(Produto produto) throws InclusaoProdutoException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = JdbcUtil.getConnection();

            final String sql = "INSERT INTO produto (nome) VALUES (?)";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, produto.getNome());

            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InclusaoProdutoException("Ocorreu uma falha ao incluir novo produto", e);
        } finally {
            JdbcUtil.close(conn, pstmt);
        }

    }

    public void atualizar(Produto produto) throws AtualizacaoProdutoException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = JdbcUtil.getConnection();

            final String sql = "UPDATE produto SET nome = ? WHERE id = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, produto.getNome());
            pstmt.setLong(2, produto.getId());

            pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new AtualizacaoProdutoException("Ocorreu uma falha ao incluir novo produto", e);
        } finally {
            JdbcUtil.close(conn, pstmt);
        }
    }

    public Produto buscarPorId(Long id) {
        Connection conn = null;
        ResultSet rs = null;
        Statement stmt = null;

        Produto produto = null;

        try {
            conn = JdbcUtil.getConnection();

            final String sql = String.format("SELECT p.id, p.nome FROM produto p WHERE p.id = %d", id);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            if(rs.next()) {
                final Long identificador = rs.getLong("id");
                final String nome = rs.getString("nome");

                produto = new Produto(identificador, nome);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtil.close(conn, rs, stmt);
        }

        return produto;
    }

    public List<Produto> buscarTodos() {
        Connection conn = null;
        ResultSet rs = null;
        Statement stmt = null;

        List<Produto> produtos = new ArrayList<Produto>();

        try {
            conn = JdbcUtil.getConnection();

            final String sql = "SELECT p.id, p.nome FROM produto p";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            while(rs.next()) {
                final Long id = rs.getLong("id");
                final String nome = rs.getString("nome");

                produtos.add(new Produto(id, nome));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtil.close(conn, rs, stmt);
        }

        return produtos;
    }

    public void excluir(Long id) throws DelecaoProdutoException {
        Connection conn = null;
        PreparedStatement pstmt = null;

        try {
            conn = JdbcUtil.getConnection();

            final String sql = "DELETE FROM produto WHERE id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DelecaoProdutoException("Ocorreu um problema ao excluir um produto", e);
        } finally {
            JdbcUtil.close(conn, pstmt);
        }
    }
}
