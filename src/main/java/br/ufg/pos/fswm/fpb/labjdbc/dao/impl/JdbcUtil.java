package br.ufg.pos.fswm.fpb.labjdbc.dao.impl;

import java.sql.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 23/06/17.
 */
class JdbcUtil {

    private static final String URL = "jdbc:postgresql://localhost:5432/bd-produto";
    private static final String USER = "postgres";
    private static final String PASS = "495tDttwhn";

    private JdbcUtil() {
        // Faz nada. Classe utilitaria;
    }

    static Connection getConnection() throws SQLException {
        return DriverManager
                .getConnection(URL, USER, PASS);
    }

    public static void close(Connection conn) {
        close(conn, null, null);
    }

    public static void close(Connection conn, Statement stmt) {
        close(conn, null, stmt);
    }

    public static void close(Connection conn, ResultSet rs) {
        close(conn, rs, null);
    }

    public static void close(Connection conn, ResultSet rs, Statement stmt) {
        try {
            if(conn != null && !conn.isClosed()) {
                conn.close();
            }
            if(stmt != null) {
                stmt.close();
            }
            if(rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            // Ai não tem o que fazer
        }
    }
}
